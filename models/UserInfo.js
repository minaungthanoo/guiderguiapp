const mongoose = require('mongoose');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const fs = require('fs');
var Schema = mongoose.Schema;

var UserGreetingSchema = new Schema({
  langCode: {
    type: String
  },
  greetingDetail: {
    type: String
  },
  certifiedList: [{
    type: String
  }]
});

var UserInfoSchema = new Schema({
  userId: mongoose.Schema.Types.ObjectId,
  name: String,
  profileImg: {
    data: Buffer
  },
  interesting: [{
    type: String
  }],
  country: {
    type: String,
    default: 'JP'
  },
  roleId : {
    type: Number,
    default: 1
  },
  recentView: {
    trip: [{
      type: mongoose.Schema.Types.ObjectId
    }],
    guider: [{
      type: mongoose.Schema.Types.ObjectId
    }]
  },
  viewCount : {
    type: Number,
    default: 0
  },
  favouriteCount : {
    type: Number,
    default: 0
  },
  rating : {
    type: Number,
    default: 0
  },
  reviewCount : {
    type: Number,
    default: 0
  },
  langSkill: [{
    langCode: {
      type: String
    },
    langName: {
      type: String
    },
    skillLevel: {
      type: Number
    },
    skillName: {
      type: String
    },
  }],
  greetingList: [UserGreetingSchema],
  createUser: mongoose.Schema.Types.ObjectId,
  createDate: {
    type: Date,
    default: Date.now
  },
  updateUser: mongoose.Schema.Types.ObjectId,
  updateDate: {
    type: Date
  }
});

UserInfoSchema.methods.toJSON = function() {
  var obj = this.toObject();
  delete obj.greetingList;
  delete obj.profileImg;
  delete obj.__v;
  delete obj.createUser;
  delete obj.createDate;
  delete obj.updateUser;
  delete obj.updateDate;
  return obj;
};

/**
 * Create new user info when user sign up from google, facebook and email.
 * @param {JSON} params {userId, name}
 * @returns {String} default profile image.
 */
UserInfoSchema.statics.createUserInfo = async function(params) {
    let userInfo = new UserInfo(params);
    userInfo.profileImg.data = fs.readFileSync('./client/public/default_img.jpg');
    await userInfo.save();
    const img = 'data:image/png;base64,' + new Buffer(userInfo.profileImg.data).toString('base64');
    return img;
};

/**
 * Get guider info.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {String} langCode
 * @returns {JSON} guider information detail.
 */
UserInfoSchema.statics.getGuiderInfo = async function (userId, langCode) {
  var UserInfo = this;
  var userObject = await UserInfo.findOne({ 'userId': userId });

  var guider = userObject.toJSON();

  // Conver image to base64.
  guider.profileImg = 'data:image/png;base64,....';// + new Buffer(userObject.profileImg.data).toString('base64');
  var greeting = _.find(userObject.greetingList, ['langCode', langCode]);
  if(greeting) {
    guider.greetingDetail = greeting.greetingDetail;
    guider.certifiedList = greeting.certifiedList;
  }
  return guider;
};

/**
 * Get a guider of trip.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @returns {JSON} guider information {userId, name, profile image}.
 */
UserInfoSchema.statics.getGuiderOfTrip = async function (userId) {
  var UserInfo = this;
  var userObject = await UserInfo.findOne({ 'userId': userId });

  return {
    userId: userId,
    name: userObject.name,
    profileImg: 'data:image/png;base64,' + new Buffer(userObject.profileImg.data).toString('base64')
  };
};

/**
 * Get followers of a guider/their follows.
 * @param {mongoose.Schema.Types.ObjectId} userIdList.
 * @returns {JSON} followers information {userId, name, profile image}.
 */
UserInfoSchema.statics.getFollowInfo = async function (userIdList) {
  var UserInfo = this;
  const followerObjectList = await UserInfo.find({userId: { $in: userIdList }});

  var followerList = [];
  for(userObject of followerObjectList) {
    let follower = {
      userId: userObject.userId,
      name: userObject.name,
      profileImg: 'data:image/png;base64,' + new Buffer(userObject.profileImg.data).toString('base64')
    };

    followerList.push(follower);
  }
  return followerList;
};

/**
 * Get recommanded guiders.
 * @param {String} langCode
 * @returns {JSON} guider information {userId, name, greeting detail and profile image}.
 */
UserInfoSchema.statics.getRecommandedGuiders = async function (langCode) {
  var UserInfo = this;
  //const recommandedObjList = await UserInfo.find({ 'roleId': 2, rating: {$gt: 3} }).sort({rating: 'desc'});

  //TODO It's only to show in phase 1.
  const recommandedObjList = await UserInfo.find({ 'roleId': 2}).sort({'createDate': 'desc'});

  var recommandedGuiderList = [];
  for(userObject of recommandedObjList) {
    let guider = {
      userId: userObject.userId,
      name: userObject.name,
      profileImg: 'data:image/png;base64,' + new Buffer(userObject.profileImg.data).toString('base64')
    };

    var greeting = _.find(userObject.greetingList, ['langCode', langCode]);
    if(greeting) {
      guider.greetingDetail = greeting.greetingDetail;
    }

    recommandedGuiderList.push(guider);
  }

  return recommandedGuiderList;
};

/**
 * Get Tourist info.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {String} langCode
 * @returns {JSON} guider information detail.
 */
UserInfoSchema.statics.getTouristInfo = async function (userId, langCode) {
  var UserInfo = this;
  var userObject = await UserInfo.findOne({ 'userId': userId });
  var tourist = userObject.toJSON();
  delete tourist.langSkill;

  // Conver image to base64.
  tourist.profileImg = 'data:image/png;base64,' + new Buffer(userObject.profileImg.data).toString('base64');
  return tourist;
};

/**
 * Update Tourist profile info.
 * @param {JSON} touristObj userId, langCode, name, interesting, country.
 * @returns {JSON} updated tourist information detail.
 */
UserInfoSchema.statics.updateTouristProfile = async function (touristObj) {
  let UserInfo = this;
  const updatedInfo = await UserInfo.update({userId: touristObj.userId}, { $set: {
    name: touristObj.name,
    interesting: touristObj.interesting,
    country: touristObj.country,
    updateUser: touristObj.userId,
    updateDate: new Date()
  }});

  return updatedInfo;
};

/**
 * Update Guider profile info.
 * @param {JSON} guiderObj userId, langCode, name, interesting, country, langSkill, certifiedList and greetingDetail.
 * @returns {JSON} updated guider information detail.
 */
UserInfoSchema.statics.updateGuiderProfile = async function (guiderObj) {
  let UserInfo = this;
  let result = await UserInfo.update({'userId': guiderObj.userId, 'greetingList.langCode': guiderObj.langCode}, { $set: {
    'name': guiderObj.name,
    'interesting': guiderObj.interesting,
    'country': guiderObj.country,
    'langSkill': guiderObj.langSkill,
    'greetingList.$.greetingDetail': guiderObj.greetingDetail,
    'greetingList.$.certifiedList': guiderObj.certifiedList,
    'updateUser': guiderObj.userId,
    'updateDate': new Date()
  }});

  // When record doesn't exist to update, insert as a new record.
  if(result.n === 0) {
    let greetingList = {
      greetingDetail: guiderObj.greetingDetail,
      certifiedList: guiderObj.certifiedList,
      langCode: guiderObj.langCode
    };
    result = await UserInfo.update({'userId': guiderObj.userId, 'greetingList.langCode':{$ne: guiderObj.langCode}},
                {
                  $push: {greetingList: greetingList},
                  $set: {
                    'updateUser': guiderObj.userId,
                    'updateDate': new Date()
                  }
                }
              );
  }

  return result;
};

/**
 * Change Tourist profile to Guider profile.
 * @param {JSON} guiderObj userId, langCode, langSkill, certifiedList and greetingDetail.
 * @returns {JSON} updated guider information detail.
 */
UserInfoSchema.statics.changeTouristToGuider = async function (guiderObj) {
  let UserInfo = this;
  const updatedInfo = await UserInfo.update({'userId': guiderObj.userId}, { $set: {
    'roleId': 2,
    'langSkill': guiderObj.langSkill,
    'greetingList': [
      {
        'langCode': guiderObj.langCode,
        'greetingDetail':  guiderObj.greetingDetail,
        'certifiedList': guiderObj.certifiedList
      }
    ],
    'updateUser': guiderObj.userId,
    'updateDate': new Date()
  }}, {new: true});

  return updatedInfo;
};

/**
 * Get login user's recent view trip/guider list.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @returns {JSON} trip id list and guider id list.
 */
UserInfoSchema.statics.getRecentView = async function (userId) {
  let UserInfo = this;
  const resultObj = await UserInfo.findOne({'userId': userId}, { 'recentView': 1, '_id': 0});
  let recentView = {
    trip: resultObj.recentView.trip.toObject(),
    guider: resultObj.recentView.guider.toObject()
  }
  return recentView;
};

/**
 * Update login user's recent viewed trip.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {mongoose.Schema.Types.ObjectId} tripId current viewed trip detail id.
 * @returns {JSON} trip id list.
 */
UserInfoSchema.statics.updateRecentViewedTrip = async function (userId, tripId) {
  let UserInfo = this;
  const resultObj = await UserInfo.findOne({'userId': userId}, { 'recentView': 1, '_id': 0});
  var recentTrip = JSON.parse(JSON.stringify( resultObj.recentView.trip));
  var recentView = recentTrip.filter(function(item) {
    return item !== tripId;
  });
  recentView.unshift(tripId);

  // Recent trip must be at most 4.
  if(recentView.length > 4) {
    // Remove the last item from array.
    recentView.splice(-1,1)
  }

  const updatedInfo = await UserInfo.update({'userId': userId}, { $set: {
    'recentView.trip': recentView
  }}, {new: true});

  return updatedInfo.ok;
};


var UserInfo = mongoose.model('UserInfo', UserInfoSchema);

module.exports = {UserInfo}
