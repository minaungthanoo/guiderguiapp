const mongoose = require('mongoose');
const _ = require('lodash');
const {Image} = require('./Image');

var StepSchema = new mongoose.Schema({
  stepImgList: [{
    type: String
  }],
  title: String,
  description: String,
  locationName: String,
  duration: Number,
  durationUnit: String,
  additionalInfo: [{
    name:String,
    value: String
  }]
});

var MultiLangTripSchema = new mongoose.Schema({
  langCode: String,
  locationName: String,
  title: String,
  description: String,
  duration : Number,
  durationUnit : String,
  minimumPeople : Number,
  price : Number,
  currencyCode: String,
  timeline: [{
    time: String,
    description: String
  }],
  stepList: [StepSchema]
});

var TripSchema = new mongoose.Schema({
  userId: mongoose.Schema.Types.ObjectId,
  imageList : [{
    type: String
  }],
  trip: [MultiLangTripSchema],
  viewCount : {
    type: Number,
    default: 0
  },
  favouriteCount : {
    type: Number,
    default: 0
  },
  rating : {
    type: Number,
    default: 0
  },
  reviewCount : {
    type: Number,
    default: 0
  },
  createUser: mongoose.Schema.Types.ObjectId,
  createDate: {
    type: Date,
    default: Date.now
  },
  updateUser: mongoose.Schema.Types.ObjectId,
  updateDate: Date
});

TripSchema.methods.toJSON = function() {
  var obj = this.toObject();
  delete obj.__v;
  delete obj.updateUser;
  delete obj.updateDate;
  delete obj.trip;
  return obj;
};

/**
 * Get all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @returns {Object} trip list.
 */
TripSchema.statics.getTripList = async function (userId, langCode) {
  var Trip = this;

  const tripObjList = await Trip.find({'userId': userId});
  var tripList = [];
  for(let tripObj of tripObjList) {
    var tripJson = tripObj.toJSON();
    const trip = _.find(tripObj.trip, ['langCode', langCode]);
    if(trip) {
      tripJson.title = trip.title;
      tripJson.description = trip.description;
      tripJson.imageList = await Image.getImageList(tripJson.imageList);
      tripList.push(tripJson);
    }
  }
  return tripList;
};

/**
 * Get all recent trip of current login user(tourist/guider).
 * @param {mongoose.Schema.Types.ObjectId} tripIdList
 * @returns {Object} trip list.
 */
TripSchema.statics.getRecentTripList = async function (tripIdList, langCode) {
  var Trip = this;
  let tripList = [];
  let tripObjList = [];

  // In case of Login user
  if(tripIdList) {
    var beforeSorting = await Trip.find({_id: { $in: tripIdList }});

    // Sorting
    for(var tripId of tripIdList) {
      var trip = _.find(beforeSorting, ['_id', tripId]);
      if(trip) {
        tripObjList.push(trip);
      }
    }
  } else {
    // In case of Guest.
    //tripObjList = await Trip.find().limit(4).sort({ 'rating': 'desc' });

    //TODO It's only to show in phase 1.
    tripObjList = await Trip.find().limit(4).sort({ 'createDate': 'desc' });
  }

  for(let tripObj of tripObjList) {

    const trip = _.find(tripObj.trip, ['langCode', langCode]);

    if(trip) {
      var tripJson = {
        tripId: tripObj._id,
        userId: tripObj.userId,
        title: trip.title,
        slogan: trip.description,
        imageList: await Image.getImageList(tripObj.imageList)
      };

      // to get last in first out.
      tripList.push(tripJson);
    }
  }
  return tripList;
};

/**
 * Get all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @returns {Object} trip list.
 */
TripSchema.statics.createNewTrip = async function (newTrip) {

  const tripObj = new Trip(newTrip);

  // Update Image for the new trip.
  let imgList = newTrip.imageList;

  // Get image of steps
  for(let obj of newTrip.trip) {
    for(let stepObj of obj.stepList)
    imgList = imgList.concat(stepObj.stepImgList);
  }

  const updateResult = await Image.updateImageInfo(newTrip.userId, imgList);

  tripObj.createUser = newTrip.userId;

  // Save new Trip
  await tripObj.save();
  return tripObj._id;
};


/**
 * Search trip by location name.
 * @param {String} locationName
 * @param {String} langCode
 * @returns {Object} trip list.
 */
TripSchema.statics.searchTrip = async function (locationName, langCode) {
  var Trip = this;
  var tripList = [];
  const regPattern = '.*' + locationName.trim() + '.*';
  const tripObjList = await Trip.find({'trip.locationName' : new RegExp(regPattern, 'i')}).sort({rating: 'desc'});;
  for(let tripObj of tripObjList) {

    const trip = _.find(tripObj.trip, ['langCode', langCode]);

    if(trip) {
      var tripJson = {
        tripId: tripObj._id,
        userId: tripObj.userId,
        locationName: tripObj.locationName,
        title: trip.title,
        slogan: trip.description,
        imageList: await Image.getImageList(tripObj.imageList)
      };

      // to get last in first out.
      tripList.push(tripJson);
    }
  }
  return tripList;
};

TripSchema.statics.updateTrip = async function (object) {
  var Trip = this;

  var totalUpdate = 0;
  for(var tripObj of object.trip) {

    var result = await Trip.update({'_id': object.tripId, 'trip.langCode': tripObj.langCode}, {
      'trip.$.title': tripObj.title,
      'trip.$.description': tripObj.description,
      'trip.$.timeline': tripObj.timeline,
      'trip.$.stepList': tripObj.stepList,
      'trip.$.locationName': tripObj.locationName,
      'trip.$.duration': tripObj.duration,
      'trip.$.durationUnit': tripObj.durationUnit,
      'trip.$.minimumPeople': tripObj.minimumPeople,
      'trip.$.price': tripObj.price,
      'trip.$.currencyCode': tripObj.currencyCode
    });

    // When record doesn't exist to update, insert as a new record.
    if(result.n === 0) {
      result = await Trip.update({'_id': object.tripId, 'trip.langCode':{$ne: tripObj.langCode}},
                    {
                      $push: { trip: tripObj}
                    });
    }

    totalUpdate += result.n;
  }

  if(totalUpdate === object.trip.length) {
    await Trip.update({'_id': object.tripId}, {
      'imageList': object.imageList,
      'updateUser': object.userId,
      'updateDate': new Date()
    });
  }

  return result.n;
};

TripSchema.statics.deleteTrip = async function (tripId) {
  var Trip = this;
  var result = await Trip.findByIdAndRemove(tripId);
  return result;
};

TripSchema.statics.getTripDetail = async function (tripId, langCode) {
  try {
    var Trip = this;

    var tripObj = await Trip.findById({'_id': tripId});
    var tripJson = tripObj.toJSON();
  
    if(langCode) {
      var trip = _.find(tripObj.trip, ['langCode', langCode]);
  
      // Get image list of each step of trip.
      for(let setp of trip.stepList) {
        setp.stepImgList = await Image.getImageList(setp.stepImgList);
      }
  
      tripJson.title = trip.title;
      tripJson.description = trip.description;
      tripJson.timeline = trip.timeline;
      tripJson.stepList = trip.stepList;
      tripJson.locationName = trip.locationName;
      tripJson.duration = trip.duration;
      tripJson.durationUnit = trip.durationUnit;
      tripJson.minimumPeople = trip.minimumPeople;
      tripJson.price = trip.price;
      tripJson.currencyCode = trip.currencyCode;
    } else {
      tripJson.trip = [];
      for(let trip of tripObj.trip) {
        trip = trip.toObject();
        // Get image list of each step of trip.
        for(let step of trip.stepList) {
          step.stepImgIdList = step.stepImgList;
          step.stepImgList = await Image.getImageList(step.stepImgList);
        }
  
        tripJson.trip.push(trip);
      }
  
      tripJson.imageIdList = tripJson.imageList;
    }
    tripJson.imageList = await Image.getImageList(tripJson.imageList);
    return tripJson;
  } catch(err) {
    return err;
  }
};

var Trip = mongoose.model('Trip', TripSchema);
module.exports = {Trip}
