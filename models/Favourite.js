const mongoose = require('mongoose');
var FavouriteSchema = new mongoose.Schema(
  {
    touristId: mongoose.Schema.Types.ObjectId,
    tripId: mongoose.Schema.Types.ObjectId
  }
);

var Favourite = mongoose.model('Favourite', FavouriteSchema);
module.exports = {Favourite};
