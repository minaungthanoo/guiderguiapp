const mongoose = require('mongoose');
var FollowSchema = new mongoose.Schema(
  {
    guiderId: mongoose.Schema.Types.ObjectId,
    touristId: mongoose.Schema.Types.ObjectId
  }
);

var Follow = mongoose.model('Follow', FollowSchema);
module.exports = {Follow};
