const mongoose = require('mongoose');
var ImageSchema = new mongoose.Schema(
  { img: {
      data: Buffer
      //contentType: String
    },
    createUser: mongoose.Schema.Types.ObjectId,
    createDate: Date,
    updateUser: mongoose.Schema.Types.ObjectId,
    updateDate: Date
  }
);

/**
 * Get all image in all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId Guider Id
 * @returns {List} Image list.
 */
ImageSchema.statics.getUserGallery = async function (userId) {
  var Image = this;
  const imgList = await Image.find({'createUser': userId});
  var resImgList = [];
  for(var binaryImg of imgList) {
    var img = 'data:image/png;base64,' + new Buffer(binaryImg.img.data).toString('base64');
    resImgList.push(img);
  }
  return resImgList;
};

ImageSchema.statics.updateImageInfo = async function (userId, imgList) {
  let updateRecord = 0;
  for(let _id of imgList) {
    await Image.findByIdAndUpdate(_id, { $set: { createUser: userId, createDate: Date.now() } });
    updateRecord++;
  }
  return updateRecord;
};

ImageSchema.statics.getImageList = async function (imageList) {
  var Image = this;
  const imgList = await Image.find({_id: { $in: imageList }});

  var resImgList = [];
  for(var binaryImg of imgList) {
    var img = 'data:image/png;base64,' + new Buffer(binaryImg.img.data).toString('base64');
    resImgList.push(img);
  }
  return resImgList;
};

var Image = mongoose.model('Image', ImageSchema);
module.exports = {Image};
