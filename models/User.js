const mongoose = require('mongoose');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;
const {UserInfo} = require('./UserInfo');

var UserSchema = new Schema({
  googleId: String,
  facebookId: String,
  email: {
    type: String,
    trim: true,
    lowercase: true
  },
  password: String,
  createUser: mongoose.Schema.Types.ObjectId,
  createDate: {
    type: Date,
    default: Date.now
  },
  updateUser: mongoose.Schema.Types.ObjectId,
  updateDate: Date
});

UserSchema.pre('save', function(next) {
  // get access to the user model
  const user = this;

  // generate a salt then run callback
  bcrypt.genSalt(10, function(err, salt) {
      if (err) { return next(err); }

      // hash (encrypt) our password using the salt
      bcrypt.hash(user.password, salt, null, function(err, hash) {
        if (err) { return next(err); }

        // overwrite plain text password with encrypted password
        user.password = hash;
        next();
      });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
      if (err) { return callback(err); }

      callback(null, isMatch);
  });
};

/**
 * Get login user info.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @returns {JSON} userId, name, profile image, role.
 */
UserSchema.statics.getLoginUserInfo = async function (userId) {
  var userInfoObj = await UserInfo.findOne({'userId': userId});
  var userObj = {
    userId: userId,
    name: userInfoObj.name,
    roleId: userInfoObj.roleId,
    profileImg: 'data:image/png;base64,' + new Buffer(userInfoObj.profileImg.data).toString('base64')
  };
  return userObj;
};

/**
 * Create new user when user sign up from google, facebook and email.
 * @param {String} params (googleId | facebookId | email & password)
 * @param {String} displayName
 * @returns {JSON} userId, name, profile image.
 */
UserSchema.statics.createUser = async function (params, displayName) {
  let currentUser = this;
  let user = new User(params);
  await user.save();

  var userObj = {
    userId: user._id,
    name: displayName
  };
  userObj.profileImg = await UserInfo.createUserInfo(userObj);

  // Set default role as tourist.
  userObj.roleId = 1;
  return userObj;
};

var User = mongoose.model('User', UserSchema);

module.exports = {User}
