const mongoose = require('mongoose');
var ChatSchema = new mongoose.Schema(
  { 
    senderId: mongoose.Schema.Types.ObjectId,
    receiverId: mongoose.Schema.Types.ObjectId,
    message: String,
    status: String,
    sendDate: Date,
    receiveDate: Date
  }
);

/**
 * Get all chatted message of both sender and receiver.
 * @param {mongoose.Schema.Types.ObjectId} senderId
 * @param {mongoose.Schema.Types.ObjectId} receiverId
 * @returns {List} Message list.
 */
ChatSchema.statics.getChatMessage = async function (senderId, receiverId) {
  var Chat = this;
  var chatList = await Chat.find( { $or: [
       { 'senderId': senderId, 'receiverId': receiverId },
       { 'senderId': receiverId, 'receiverId': senderId }
      ] }, {'_id': 0, '__v': 0} );

  return chatList;
};

var Chat = mongoose.model('Chat', ChatSchema);
module.exports = {Chat};
