const express = require('express');
const imgRouter = express.Router();
let multer = require('multer');
const {Image} = require('../models/Image');
const {UserInfo} = require('../models/UserInfo');
const {ObjectID} = require('mongodb');
let upload  = multer({ storage: multer.memoryStorage() });

/**
 * Update user profile image.
 * @param {file} file single file
 * @returns HTTP status 200.
 */
imgRouter.post('/profileImage/:userId', upload.single('file'), async function(req, res) {
  const userId = req.params.userId;

  // Bad Request from client
  if (!req.file || !ObjectID.isValid(userId)) {
    return res.status(400).send();
  }

  const updatedInfo = await UserInfo.findOneAndUpdate({userId: userId}, { $set: {
                        profileImg: {data:req.file.buffer},
                        updateUser: userId,
                        updateDate: new Date()
                      }}, {new: true});

  // In case of update successful.
  if(updatedInfo) {
    res.send();
  }

  // Internal server error.
  res.status(500).send();
});

/**
 * Tempory save user uploaded images. That images can be trip, step of trip.
 * @param {file} file file list
 * @returns Image Id list.
 */
imgRouter.post('/photo', upload.array('file'), function(req, res) {

  // Bad Request from client
  if (!req.files) {
    return res.status(400).send();
  }

  var imageIdList = [];
  for (var file of req.files) {
   var newItem = new Image();
   newItem.img.data = file.buffer;
   newItem.save();
   imageIdList.push(newItem._id);
  }

 res.send(imageIdList);
});

/**
 * Get all image in all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId Guider Id
 * @returns {List} Image list.
 */
imgRouter.post('/getUserGallery', async (req, res) => {
  try {
    const userId = req.body.userId;

    if (!ObjectID.isValid(userId)) {
      return res.status(400).send();
    }

    const imgList = await Image.getUserGallery(userId);
    res.send(imgList);
  } catch(e) {
    res.status(400).send(e);
  }
});

// TODO to check authenticate
imgRouter.get('/clear', async function(req, res) {
  await Image.remove({createUser: undefined});
  res.send();
});

imgRouter.get('/photo/:imageId', async function(req, res) {
  var imageId = req.params.imageId;

  if (!ObjectID.isValid(imageId)) {
    return res.status(400).send();
  }

  var results = await Image.findById({'_id': imageId});
  res.setHeader('content-type', 'image/png');//results.img.contentType);
  res.send(results.img.data);
});

// TODO to remove, it is only for testing.
imgRouter.get('/imglist', async function(req, res) {
  var imgList = ['5a3340343c62592e24537fdb', '5a3340c22d6c85296c221f67'];
  var mm = await Image.getImageList(imgList);
  res.send(mm);
});

// TODO to remove, it is only for testing.
imgRouter.post('/updateImageInfo', async (req, res) => {
  try {
    const userId = req.body.userId;
    if (!ObjectID.isValid(userId)) {
      return res.status(400).send();
    }

    await Image.updateImageInfo(userId, req.body.imgList);
    res.send();
  } catch(e) {
    res.status(400).send(e);
  }
});

module.exports = imgRouter;
