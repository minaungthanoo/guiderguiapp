const _ = require('lodash');
const {ObjectID} = require('mongodb');
const {UserInfo} = require('../models/UserInfo');
const {Trip} = require('../models/Trip');
const {Chat} = require('../models/Chat');
const {Follow} = require('../models/Follow');
const {Favourite} = require('../models/Favourite');
const {authenticate} = require('../services/middleware/authenticate');
const express = require('express');
const router = express.Router();

// TODO : it's not sure user or not. Get guider information to use in trip.
router.post('/getGuider', async (req, res) => {
  try {
    const body = _.pick(req.body, ['userId', 'guiderId', 'langCode']);

    if (!ObjectID.isValid(body.guiderId)) {
      return res.status(400).send();
    }

    let user = await UserInfo.getGuiderInfo(body.guiderId, body.langCode);

    if(body.userId && user) {
      if (!ObjectID.isValid(body.userId)) {
        return res.status(400).send();
      }

      user.isFollow = await Follow.count({'guiderId': body.guiderId, 'touristId': body.userId});
    }
    res.send(user);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get Recommanded Guiders.
 * @param {String} langCode
 * @returns {JSON} guiders {userId, name, greetingDetail, profileImg}.
 */
router.post('/getRecommandedGuiders', async (req, res) => {
  try {
    const recommandedGuiderList = await UserInfo.getRecommandedGuiders(req.body.langCode);
    res.send(recommandedGuiderList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get Tourist info when roleId is equal to 1.
 * Get Guider info when roleId is equal to 2.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {String} langCode
 * @returns {JSON} guider information detail.
 */
router.post('/getUserProfileInfo', async (req, res) => {
  try {
    const body = _.pick(req.body, ['userId', 'roleId', 'langCode']);

    if (!ObjectID.isValid(body.userId)) {
      return res.status(400).send();
    }

    var userProfile;
    // In case of tourist.
    if(body.roleId === 1) {
      userProfile = await UserInfo.getTouristInfo(body.userId, body.langCode);
    } else {
      userProfile = await UserInfo.getGuiderInfo(body.userId, body.langCode);
    }

    res.send(userProfile);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Update Tourist info when roleId is equal to 1.
 * Update Guider info when roleId is equal to 2.
 * @param {JSON} userObject It can be tourist/guider json object.
 * @returns HTTP success status.
 */
router.post('/updateUserProfile', async (req, res) => {
  try {
    let userObj = req.body;
    if (!ObjectID.isValid(userObj.userId)) {
      return res.status(400).send();
    }

    let updatedInfo;

    // In case of tourist.
    if(userObj.roleId === 1) {
      updatedInfo = await UserInfo.updateTouristProfile(userObj);
    } else {
      updatedInfo = await UserInfo.updateGuiderProfile(userObj);
    }

    // In case of update successful.
    if(updatedInfo.n === 1) {
      res.send();
    }
  
    // Internal server error.
    res.status(500).send();
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * When Tourist want to be a guider and create trip.
 * Change Tourist role to Guider. Tourist have to register guider info.
 * @param {JSON} guiderObject userId, langCode, langSkill, certifiedList and greetingDetail.
 * @returns {JSON} updated guiderObject
 */
router.post('/changeTouristToGuider', async (req, res) => {
  try {
    let userObj = req.body;
    if (!ObjectID.isValid(userObj.userId)) {
      return res.status(400).send();
    }

    let updatedInfo;

    updatedInfo = await UserInfo.changeTouristToGuider(userObj);

    // In case of update successful.
    if(updatedInfo.n === 1) {
      const guider = await UserInfo.getGuiderInfo(userObj.userId, userObj.langCode);
      res.send(guider);
    }
  
    // Internal server error.
    res.status(500).send();
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get current user's recent viewed trips and guiders.
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {String} langCode
 * @returns {JSON} recent view list.
 */
router.post('/getRecentView', async (req, res) => {
  try {
    let userId = req.body.userId;
    let recentList;
    let tripIdList;
    if (!ObjectID.isValid(userId)) {
      return res.status(400).send();
    }

    // Get trip id list from current login user.
    const recentView = await UserInfo.getRecentView(userId);
    tripIdList = recentView.trip;
  
    // pick up trip id list and get recent trip list.
    recentList = await Trip.getRecentTripList(tripIdList , req.body.langCode);

      // TODO next phase, pick up guider id list and get guider list.
      //const guiderIdList = recentView.guider;

    res.send(recentList);

  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get recommanded trips.
 * @param {String} langCode
 * @returns {JSON} recommanded trip list.
 */
router.post('/getRecommandedTrip', async (req, res) => {
  try {
  
    // pick up trip id list and get recent trip list.
    let recentList = await Trip.getRecentTripList(null , req.body.langCode);

      // TODO next phase, pick up guider id list and get guider list.
      //const guiderIdList = recentView.guider;

    res.send(recentList);

  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get trip detail when Guest view(click) on trip.
 * userId is Current login user id to save their recent view trip.
 * @param {mongoose.Schema.Types.ObjectId} tripId
 * @param {mongoose.Schema.Types.ObjectId} userId
 * @param {String} langCode
 * @returns {JSON} trip detail that contained guider info.
 */
router.post('/viewTripDetail', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.tripId)) {
      return res.status(400).send();
    }

    const loginUserId = req.body.userId;

    // Update recent view of trip.
    if (loginUserId) {

      if (!ObjectID.isValid(loginUserId)) {
        return res.status(400).send();
      }

      await UserInfo.updateRecentViewedTrip(loginUserId, req.body.tripId);
    }

    // Get trip info.
    let trip = await Trip.getTripDetail(req.body.tripId, req.body.langCode);

    // Get a guider of trip.
    let guider = await UserInfo.getGuiderOfTrip(trip.userId);

    if (req.body.userId) {
      guider.isFollow = await Follow.count({'guiderId': trip.userId, 'touristId': loginUserId});
      trip.isFavourite = await Favourite.count({'touristId': loginUserId, 'tripId': req.body.tripId});
    }

    let tripDetail = {
      guider: guider,
      trip: trip
    };
    res.send(tripDetail);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Save send message of sender
 * @param {mongoose.Schema.Types.ObjectId} senderId
 * @param {mongoose.Schema.Types.ObjectId} receiverId
 * @param {String} message
 * @returns {JSON} Chat Object.
 */
router.post('/sendMessage', async (req, res) => {
  try {
    let chatObj = req.body;
    if (!ObjectID.isValid(chatObj.senderId) || !ObjectID.isValid(chatObj.receiverId)) {
      return res.status(400).send();
    }

    chatObj.status = "sent";
    chatObj.sendDate = new Date();
    chatObj.receiveDate = new Date();
    var chat = new Chat(chatObj);

    await chat.save();

    res.send(chat);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get all chatted message of both sender and receiver.
 * @param {mongoose.Schema.Types.ObjectId} senderId
 * @param {mongoose.Schema.Types.ObjectId} receiverId
 * @returns {List} Message list.
 */
router.post('/getChatMessage', async (req, res) => {
  try {
    let chatObj = req.body;
    if (!ObjectID.isValid(chatObj.senderId) || !ObjectID.isValid(chatObj.receiverId)) {
      return res.status(400).send();
    }

    const chatList = await Chat.getChatMessage(chatObj.senderId, chatObj.receiverId);

    res.send(chatList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Search trip by location name.
 * @param {String} locationName
 * @param {String} langCode
 * @returns {Object} trip list.
 */
router.post('/searchTrip', async (req, res) => {
  try {
    const trip = await Trip.searchTrip(req.body.locationName, req.body.langCode);
    res.send(trip);
  } catch(e) {
    res.status(400).send(e);
  }
});

router.post('/createTrip', async (req, res) => {
  try {
    let newTrip = req.body;
    if (!ObjectID.isValid(req.body.userId)) {
      return res.status(400).send();
    }
    
    const newTripId = await Trip.createNewTrip(newTrip);
    res.send({tripId: newTripId});
  } catch(e) {
    res.status(400).send(e);
  }
});

router.post('/updateTrip', async (req, res) => {
  try {
    let updateTrip = req.body;
    if (!ObjectID.isValid(updateTrip.userId) || !ObjectID.isValid(updateTrip.tripId)) {
      return res.status(400).send();
    }
    const trip = await Trip.updateTrip(updateTrip);
    res.send(trip);
  } catch(e) {
    res.status(400).send(e);
  }
});

router.post('/deleteTrip', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.tripId)) {
      return res.status(400).send();
    }
    const result = await Trip.deleteTrip(req.body.tripId);

    if(result) {
      // delete success..
      res.send();
    }
    res.status(400).send();
  } catch(e) {
    res.status(400).send(e);
  }
});


// TODO : it's not sure use or not. 
router.post('/getTripDetail', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.tripId)) {
      return res.status(404).send();
    }

    const trip = await Trip.getTripDetail(req.body.tripId, req.body.langCode);
    res.send(trip);
  } catch(e) {
    res.status(400).send(e);
  }
});

// Trip List of a Guider.
router.post('/getTripList', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.userId)) {
      return res.status(404).send();
    }

    const tripList = await Trip.getTripList(req.body.userId, req.body.langCode);
    res.send(tripList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get followers of a guider.
 * @param {mongoose.Schema.Types.ObjectId} guiderId Current login user id.
 * @returns {JSON} followers information {userId, name, profile image}.
 */
router.post('/getFollowers', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.guiderId)) {
      return res.status(404).send();
    }

    const resList = await Follow.find({'guiderId': req.body.guiderId});

    let followerIdList = [];
    for(let obj of resList) {
      followerIdList.push(obj.touristId);
    }

    // In case of no followers.
    if(followerIdList.length < 1) {
      res.send(followerIdList);
    }

    // Get follows detail information
    const followerList = await UserInfo.getFollowInfo(followerIdList);
    res.send(followerList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get follows of a guider.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @returns {JSON} follows information {userId, name, profile image}.
 */
router.post('/getFollows', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId)) {
      return res.status(404).send();
    }

    const resList = await Follow.find({'touristId': req.body.touristId});

    let followIdList = [];
    for(let obj of resList) {
      followIdList.push(obj.guiderId);
    }

    // In case of no follows.
    if(followIdList.length < 1) {
      res.send(followIdList);
    }

    // Get follows detail information
    const followList = await UserInfo.getFollowInfo(followIdList);
    res.send(followList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Create user follow of a guider.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @param {mongoose.Schema.Types.ObjectId} guiderId.
 * @returns HTTPS success status.
 */
router.post('/createFollow', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId)) {
      return res.status(404).send();
    }
    if (!ObjectID.isValid(req.body.guiderId)) {
      return res.status(404).send();
    }
    const follow = new Follow({touristId: req.body.touristId, guiderId: req.body.guiderId});
    await follow.save();
    res.send();
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Unfollow of a guider.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @param {mongoose.Schema.Types.ObjectId} guiderId.
 * @returns HTTPS success status.
 */
router.post('/unFollow', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId)) {
      return res.status(404).send();
    }

    if (!ObjectID.isValid(req.body.guiderId)) {
      return res.status(404).send();
    }
    await Follow.remove({touristId: req.body.touristId, guiderId: req.body.guiderId});
    res.send();
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Get all favourite trip of current login user.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @returns {JSON} follows information {userId, name, profile image}.
 */
router.post('/getFavouriteTrip', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId)) {
      return res.status(404).send();
    }

    const favouriteTripIdList = await Favourite.find({'touristId': req.body.touristId});

    res.send(favouriteTripIdList);
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Create user favouriet of a trip.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @param {mongoose.Schema.Types.ObjectId} tripId.
 * @returns HTTPS success status.
 */
router.post('/createFavourite', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId) || !ObjectID.isValid(req.body.tripId)) {
      return res.status(404).send();
    }

    const favouriet = new Favourite({touristId: req.body.touristId, tripId: req.body.tripId});
    await favouriet.save();
    res.send();
  } catch(e) {
    res.status(400).send(e);
  }
});

/**
 * Remove favourite of a trip.
 * @param {mongoose.Schema.Types.ObjectId} touristId Current login user id.
 * @param {mongoose.Schema.Types.ObjectId} tripId.
 * @returns HTTPS success status.
 */
router.post('/removeFavourite', async (req, res) => {
  try {
    if (!ObjectID.isValid(req.body.touristId) || !ObjectID.isValid(req.body.tripId)) {
      return res.status(404).send();
    }

    await Favourite.remove({touristId: req.body.touristId, tripId: req.body.tripId});
    res.send();
  } catch(e) {
    res.status(400).send(e);
  }
});

module.exports = router;
