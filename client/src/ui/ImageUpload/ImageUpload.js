import React, { Component } from "react";
import Auxi from "../../hoc/Auxi";

import "./ImageUpload.css";

class ImageUpload extends React.Component {
	constructor(props) {
		super(props);
		this.state = { file: "", imagePreviewUrl: "" };
	}

	handleSubmit(e) {
		e.preventDefault();
		// TODO: do something with -> this.state.file
		console.log("handle uploading-", this.state.file);
	}

	handleImageChange(e) {
		e.preventDefault();

		let reader = new FileReader();
		let file = e.target.files[0];

		reader.onloadend = () => {
			this.setState({
				file: file,
				imagePreviewUrl: reader.result
			});
		};

		reader.readAsDataURL(file);
	}

	render() {
		let { imagePreviewUrl } = this.state;
		let $imagePreview = null;
		if (imagePreviewUrl) {
			$imagePreview = (
				<img className={this.props.imgClass} src={imagePreviewUrl} />
			);
		} else if (this.props.imageUrl) {
			$imagePreview = (
				<img className={this.props.imgClass} src={this.props.imageUrl} />
			);
		} else {
			$imagePreview = (
				<div className="previewText">Please select an Image for Preview</div>
			);
		}

		return (
			<Auxi>
				<div className={this.props.imgDivClass}>{$imagePreview}</div>
				<form className="g-text-center" onSubmit={e => this.handleSubmit(e)}>
					<label className="fileContainer">
						<i className="fa fa-pencil-square-o" aria-hidden="true" />Edit
						<input type="file" onChange={e => this.handleImageChange(e)} />
					</label>

					<label className="fileContainer">
						<i className="fa fa-floppy-o" aria-hidden="true" />Save
						<input type="submit" onClick={e => this.handleSubmit(e)} />
					</label>
				</form>
			</Auxi>
		);
	}
}

export default ImageUpload;
