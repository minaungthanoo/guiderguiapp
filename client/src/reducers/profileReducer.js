import { FETCH_GUIDER, FETCH_USER_PROFILEINFO } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case FETCH_GUIDER:
            return action.payload;
        case FETCH_USER_PROFILEINFO:
            return action.payload;
        default:
            return state;
    }
}