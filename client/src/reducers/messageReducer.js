import { FETCH_CHAT_MESSAGE, SEND_MESSAGE } from '../actions/types';

const INITIAL_STATE = { all: [], message: null };

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case FETCH_CHAT_MESSAGE:
			return { ...state, all:action.payload.data };
        case SEND_MESSAGE:
			return { ...state, message:action.payload.data };
        default:
            return state;
    }
}