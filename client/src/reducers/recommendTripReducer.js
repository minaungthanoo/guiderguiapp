import { FETCH_RECOMMENDED_TRIP } from "../actions/types";

export default function(state = [], action) {
	switch (action.type) {
		case FETCH_RECOMMENDED_TRIP:
			return action.payload;
		default:
			return state;
	}
}
