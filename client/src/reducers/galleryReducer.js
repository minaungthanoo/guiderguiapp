import { FETCH_USERGALLERY, SELECTED_IMAGE } from '../actions/types';

const INITIAL_STATE = { gallery: [], image: null };

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case FETCH_USERGALLERY:
            return { ...state, gallery: action.payload.data };
        case SELECTED_IMAGE:
            return { ...state, image: action.payload };
        default:
            return state;
    }
}