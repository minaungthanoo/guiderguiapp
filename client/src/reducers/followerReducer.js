import { FETCTH_FOLLOWERS } from '../actions/types';

export default function(state = [], action) {
    switch(action.type) {
        case FETCTH_FOLLOWERS:
            return action.payload;
        default:
            return state;
    }
}