import { FETCTH_FOLLOWS } from "../actions/types";

export default function(state = [], action) {
	switch (action.type) {
		case FETCTH_FOLLOWS:
			return action.payload;
		default:
			return state;
	}
}
