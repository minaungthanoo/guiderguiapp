import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import authReducer from "./authReducer";
import profileReducer from "./profileReducer";
import tripReducer from "./tripReducer";
import galleryReducer from "./galleryReducer";
import recentSearchReducer from "./recentSearchReducer";
import recommendedGuideReducer from "./recommendedGuideReducer";
import recommendedTripReducer from "./recommendTripReducer";
import searchResultReducer from "./searchResultReducer";
import followerReduer from "./followerReducer";
import followsReduer from "./followsReducer";
import messageReducer from "./messageReducer";

export default combineReducers({
	form,
	auth: authReducer,
	profile: profileReducer,
	trips: tripReducer,
	gallery: galleryReducer,
	recentSearch: recentSearchReducer,
	guides: recommendedGuideReducer,
	recommdedTrips: recommendedTripReducer,
	searchResult: searchResultReducer,
	followers: followerReduer,
	following: followsReduer,
	messages: messageReducer
});
