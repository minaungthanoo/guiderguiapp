import { FETCH_TRIPLIST, FETCH_TRIPDETAIL } from '../actions/types';

const INITIAL_STATE = { all: [], trip: null };

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case FETCH_TRIPDETAIL:
            return { ...state, trip:action.payload.data };
        case FETCH_TRIPLIST:
            return { ...state, all:action.payload.data };
        default:
            return state;
    }
}