import { FETCH_RECOMMENDED_GUIDERS } from '../actions/types';

export default function(state = [], action) {
    switch(action.type) {
        case FETCH_RECOMMENDED_GUIDERS:
            return action.payload;
        default:
            return state;
    }
}