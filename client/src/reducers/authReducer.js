import { 
  FETCH_USER,
  LOGIN,
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_MESSAGE
} from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
      case AUTH_USER:
        return action.payload;
      case UNAUTH_USER:
        return action.payload;
      case AUTH_ERROR:
        return { ...state, error: action.payload };
      case FETCH_MESSAGE:
        return { ...state, message: action.payload };
      case FETCH_USER:
        return action.payload || false;
      case LOGIN:
        return action.payload;
      default:
        return state;
    }
}
