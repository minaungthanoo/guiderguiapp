import { FETCH_RECENT_VIEW } from '../actions/types';

export default function(state = [], action) {
    switch(action.type) {
        case FETCH_RECENT_VIEW:
            return action.payload;
        default:
            return state;
    }
}