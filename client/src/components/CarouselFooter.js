import React from "react";

const CarouselFooter = () => {
  
  return (
    <div className="g-block">
      <div className="container">
        <div className="row">
          <div className="col s12 m6 l4 xl4  g-spacing">
            <div className="g-border">
                <h5 className="g-color">Some header</h5>
                <p className="g-margin-1">The description goes here</p>
            </div>
          </div>
          <div className="col s12 m6 l4 xl4  g-spacing">
            <div className="g-border">
                  <h5 className="g-color">Some header</h5>
                  <p className="g-margin-1">The description goes here</p>
              </div>
            </div>
           <div className="col s12 m6 l4 xl4  g-spacing">
            <div className="g-border">
                <h5 className="g-color">Some header</h5>
                <p className="g-margin-1">The description goes here</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CarouselFooter;