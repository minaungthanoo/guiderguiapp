import React, { Component } from "react";
import { Button } from "antd";
import { connect } from "react-redux";
import * as actions from "../actions";

class SearchBar extends Component {
	constructor(props) {
		super(props);

		this.state = { term: "" };
	}

	render() {
		return (
			<div className="g-search">
				<div className="container row">
					<input
						type="text"
						id="g-search-input"
						className="col s8"
						onChange={this.onInputChange}
						onKeyDown={this.onPressEnter}
					/>
					<Button
						id="g-btn-search"
						className="col s2"
						type="primary"
						icon="search"
						onClick={this.onSearch}>
						Search
					</Button>
				</div>
			</div>
		);
	}

	onInputChange = e => {
		console.log(e.target.value);
		this.setState({ term: e.target.value });
	};

	onSearch = () => {
		this.searchTrip(this.state.term);
	};

	onPressEnter = e => {
		if (e.keyCode === 13) {
			this.searchTrip(this.state.term);
		}
	};

	searchTrip(term) {
		this.props.fetchSearchResult({ locationName: term, langCode: "ja" });
	}
}

function mapStateToProps(state) {
	return {
		searchResult: state.searchResult
	};
}

export default connect(mapStateToProps, actions)(SearchBar);
