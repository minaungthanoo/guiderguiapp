import React from "react";

const Footer = () => {

  return (
    <div>
        <div className="g-footer">
        </div>         
        <div className="g-copyright">
            &copy;2017 Guider All right Reserved.
        </div>
    </div>
  );
};

export default Footer;
