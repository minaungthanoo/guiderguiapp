import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.login('minaungthanoo@gmail.com', "password!");
    }

    render() {
        return (
            <div>
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>Email</label>
                    <input type="text" className="form-control" name="email"/>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password"/>
                </div>

                <button type="submit" className="btn btn-warning btn-lg">Login</button>
            </form>
            </div>
        )
    }
}

export default connect(null, actions)(Login);
