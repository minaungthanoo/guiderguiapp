import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import _ from "lodash";
import { Icon } from "antd";
import Carousel from "./Carousel";
import CarouselFooter from "./CarouselFooter";
import ContentBottom from "./ContentBottom";
import SearchBar from "./SearchBar";

class Landing extends Component {
	componentDidMount() {
		if (this.props.user.userId) {
			this.props.fetchRecentView({
				userId: this.props.user.userId,
				langCode: "ja"
			});
		}

		this.props.fetchRecommendedGuiders({ langCode: "ja" });
		this.props.fetchRecommendedTrip({ langCode: "ja" });
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.user !== nextProps.user) {
			this.props.fetchRecentView({
				userId: nextProps.user.userId,
				langCode: "ja"
			});
		}
	}

	render() {
		return (
			<div>
				<div className="g-carousel">
					<Carousel />
					<SearchBar />
				</div>
				<CarouselFooter />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		recentSearch: state.recentSearch,
		guiders: state.guides
	};
}

export default connect(mapStateToProps, actions)(Landing);
