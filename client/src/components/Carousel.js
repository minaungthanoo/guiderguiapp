import React from "react";
import { Carousel } from 'antd';

import img1 from '../image/image1.jpg';
import img2 from '../image/image2.jpg';
import img3 from '../image/image5.jpg';
import img4 from '../image/image6.jpg';

const Carousels = () => {
  
  return (
    <Carousel autoplay>
      <div><img className="g-carousel-img" alt="example" src={img1} /></div>
      <div><img className="g-carousel-img" alt="example" src={img2} /></div>
      <div><img className="g-carousel-img" alt="example" src={img3} /></div>
      <div><img className="g-carousel-img" alt="example" src={img4} /></div>
    </Carousel>
  );
};

export default Carousels;