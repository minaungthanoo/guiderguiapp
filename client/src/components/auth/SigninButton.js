import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form'; 
import { Button } from 'antd';

const SigninButton = ({ dispatch }) => (
  <Button
    className="g-btn-submit g-float-r" type="primary"
    onClick={() => dispatch(submit('signin'))}
  >
    Signin
  </Button>
);

export default connect()(SigninButton);
