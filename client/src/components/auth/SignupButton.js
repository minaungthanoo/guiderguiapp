import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form'; 
import { Button } from 'antd';

const SignupButton = ({ dispatch }) => (
  <Button
    className="g-btn-submit g-float-r" type="primary"
    onClick={() => dispatch(submit('signup'))}
  >
    Signup
  </Button>
);

export default connect()(SignupButton);
