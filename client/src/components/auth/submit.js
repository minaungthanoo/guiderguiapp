import { SubmissionError } from 'redux-form';

export default function submit(values) {
	if (!['john', 'paul', 'george', 'ringo'].includes(values.email)) {
	throw new SubmissionError({
		email: 'User does not exist',
		_error: 'Login failed!',
	});
	} else if (values.password !== 'redux-form') {
	throw new SubmissionError({
		password: 'Wrong password',
		_error: 'Login failed!',
	});
	} else {
		window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
	}
};
