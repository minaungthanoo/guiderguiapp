import React, { Component } from "react";
import { Form, Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Modal, Button } from "antd";
import * as actions from "../../actions";
import SigninButton from "./SigninButton";
import { SubmissionError } from "redux-form";

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
		<span className="g-form-required">{label}</span>
		<input {...input} type={type} />
		{touched && error && <span className="error">{error}</span>}
	</div>
);

//guiderjpadmin@gmail.com
//admin1234
class Signin extends Component {
	state = {
		visible: false
	};

	showModal = () => {
		this.setState({
			visible: true
		});
	};

	handleCancel = () => {
		this.setState({ visible: false });
	};

	handleFormSubmit({ email, password }) {
		console.log(email, password);
		if (!email) {
			throw new SubmissionError({
				email: "Email is required"
			});
		}

		if (!password) {
			throw new SubmissionError({
				password: "Password is required"
			});
		}
		// Need to do something to log user in
		return this.props
			.signinUser({ email, password }, this.props.history)
			.catch(error => {
				throw new SubmissionError({
					_error: "Login Failed"
				});
			});
	}

	render() {
		const { visible } = this.state;

		const { error, handleSubmit } = this.props;

		return (
			<div>
				<Button className="g-btn" onClick={this.showModal}>
					Sign In
				</Button>
				<Modal
					visible={visible}
					title="Signin"
					onCancel={this.handleCancel}
					footer={null}>
					<div className="row g-margin-b0">
						<div className="col s12 m6 l6 xl6">
							<span>Login with social media</span>
							<Button icon="facebook" href="/auth/facebook" className="btn-fb">
								Facebook
							</Button>
							<Button icon="google" href="/auth/google" className="btn-google">
								Google
							</Button>
							<Button icon="mail" className="btn-email">
								E-mail
							</Button>
						</div>
						<div className="col s12 m6 l6 xl6 verticalLine">
							<span>Login with account</span>
							<Form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
								<Field
									name="email"
									type="email"
									component={renderField}
									label="Email"
								/>
								<Field
									name="password"
									type="password"
									component={renderField}
									label="Password"
								/>
								{error && <strong>{error}</strong>}
							</Form>
							<SigninButton />
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { errorMessage: state.auth.error };
}

Signin = reduxForm({
	form: "signin" // a unique identifier for this form
})(Signin);
export default connect(mapStateToProps, actions)(withRouter(Signin));
