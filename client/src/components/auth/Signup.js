import React, { Component } from "react";
import { reduxForm, Field, Form } from "redux-form";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Modal, Button } from "antd";
import * as actions from "../../actions";
import { SubmissionError } from "redux-form";
import SignupButton from "./SignupButton";

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div>
		<span className="g-form-required">{label}</span>
		<input {...input} type={type} />
		{touched && error && <span className="error">{error}</span>}
	</div>
);
const email = value =>
	value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
		? "Invalid email address"
		: undefined;

const aol = value =>
	value && /.+@aol\.com/.test(value)
		? "Really? You still use AOL for your email?"
		: undefined;

class Signup extends Component {
	state = {
		visible: false
	};

	showModal = () => {
		this.setState({
			visible: true
		});
	};

	handleCancel = () => {
		this.setState({ visible: false });
	};

	handleSubmit(formProps) {
		if (!formProps.name) {
			throw new SubmissionError({
				name: "Username is required"
			});
		}

		if (!formProps.email) {
			throw new SubmissionError({
				email: "email is required"
			});
		}

		if (!formProps.password) {
			throw new SubmissionError({
				password: "Password is required"
			});
		}

		if (!formProps.passwordConfirm) {
			throw new SubmissionError({
				passwordConfirm: "Confirm Password is required"
			});
		}

		if (formProps.password !== formProps.passwordConfirm) {
			throw new SubmissionError({
				password: "Passwords must match"
			});
		}

		// Call action create to signup the user
		return this.props.signupUser(formProps, this.props.history).catch(error => {
			throw new SubmissionError({
				_error: error.response.data.error
			});
		});
	}

	render() {
		const { visible } = this.state;

		const { error, handleSubmit } = this.props;

		return (
			<div>
				<Button className="g-btn" onClick={this.showModal}>
					Sign Up
				</Button>
				<Modal
					visible={visible}
					title="Sign Up"
					onOk={this.handleOk}
					onCancel={this.handleCancel}
					footer={null}>
					<Form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
						<Field
							name="name"
							label="Username"
							type="text"
							component={renderField}
						/>
						<Field
							name="email"
							label="Email"
							type="email"
							component={renderField}
							validate={email}
							warn={aol}
						/>
						<Field
							name="password"
							label="Password"
							type="password"
							component={renderField}
						/>
						<Field
							name="passwordConfirm"
							label="Comfirm Password"
							type="password"
							component={renderField}
						/>
						{error && <strong>{error}</strong>}

						<div className="g-margin-b2">
							<SignupButton />
						</div>
					</Form>
				</Modal>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { errorMessage: state.auth.error };
}

Signup = reduxForm({
	form: "signup"
})(Signup);
export default connect(mapStateToProps, actions)(withRouter(Signup));
