export const FETCH_USER = "fetch_user";
export const LOGIN = "login";
export const AUTH_USER = "auth_user";
export const UNAUTH_USER = "unauth_user";
export const AUTH_ERROR = "auth_error";
export const FETCH_MESSAGE = "fetch_message";
export const FETCH_GUIDER = "fetch_guider";
export const FETCH_TRIPLIST = "fetch_triplist";
export const FETCH_TRIPDETAIL = "fetch_tripDetail";
export const CREATE_TRIP = "create_trip";
export const UPDATE_TRIP = "update_trip";
export const FETCH_USERGALLERY = "fetch_userGallery";
export const SELECTED_IMAGE = "selected_image";
export const UPDATE_PROFILE = "update_profile";
export const FETCH_RECENT_VIEW = "get_recentview";
export const FETCH_RECOMMENDED_GUIDERS = "get_recommended_guiders";
export const FETCH_RECOMMENDED_TRIP = "get_recommended_trip";
export const FETCH_USER_PROFILEINFO = "get_user_profile_info";
export const FETCH_SEARCH_RESULT = "search_trip";
export const FETCTH_FOLLOWERS = "get_followers";
export const FETCTH_FOLLOWS = "get_follows";
export const CHANGE_TOURIST_TO_GUIDER = "change_tourist_to_guider";
export const FOLLOW = "follow";
export const UNFOLLOW = "unfollow";
export const FETCH_CHAT_MESSAGE = "get_chat_message";
export const SEND_MESSAGE = "send_message";