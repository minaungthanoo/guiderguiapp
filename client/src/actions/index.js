import axios from "axios";
import {
	AUTH_USER,
	UNAUTH_USER,
	AUTH_ERROR,
	FETCH_MESSAGE,
	FETCH_USER,
	FETCH_GUIDER,
	FETCH_TRIPLIST,
	FETCH_TRIPDETAIL,
	FETCH_USERGALLERY,
	SELECTED_IMAGE,
	CREATE_TRIP,
	UPDATE_PROFILE,
	FETCH_RECENT_VIEW,
	FETCH_RECOMMENDED_GUIDERS,
	FETCH_RECOMMENDED_TRIP,
	FETCH_USER_PROFILEINFO,
	FETCH_SEARCH_RESULT,
	FETCTH_FOLLOWERS,
	FETCTH_FOLLOWS,
	CHANGE_TOURIST_TO_GUIDER,
    UPDATE_TRIP,
	FOLLOW,
	UNFOLLOW,
	FETCH_CHAT_MESSAGE,
	SEND_MESSAGE
} from "./types";

export const fetchUser = () => async dispatch => {
	const res = await axios.get("/api/current_user");
	//console.log('current user ' + JSON.stringify(res))
	dispatch({ type: FETCH_USER, payload: res.data });
};

export function signinUser({ email, password }, history) {
	return function(dispatch) {
		return new Promise((resolve, reject) => {
			// Submit email/password to the server
			axios
				.post("/api/signin", { email, password })
				.then(response => {
					// If request is good...
					// - Update state to indicate user is authenticated
					dispatch({ type: AUTH_USER, payload: response.data.user });

					// - Save the JWT token
					localStorage.setItem("token", response.data.token);
					// - redirect to the route '/feature'
					//history.push('/dashboard');
					resolve();
				})
				.catch(err => {
					// If request is bad...
					// - Show an error to the user

					reject(err);
					//dispatch(authError('Bad Login Info'))
				});
		});
	};
}

export function signupUser({ email, password, name }, history) {
	return function(dispatch) {
		return new Promise((resolve, reject) => {
			axios
				.post("/api/signup", { email, password, name })
				.then(response => {
					dispatch({ type: AUTH_USER, payload: response.data.user });
					localStorage.setItem("token", response.data.token);
					history.push("/");
					resolve();
				})
				.catch(err => {
					reject(err);
					//dispatch(authError(response.data.error)
				});
		});
	};
}

export function authError(error) {
	return {
		type: AUTH_ERROR,
		payload: error
	};
}

export function signoutUser(history) {
	return function(dispatch) {
		if (localStorage.getItem("token")) {
			localStorage.removeItem("token");
		}
		return dispatch(signout(history));
	};
}

export function signout(history) {
	return function(dispatch) {
		axios.get("/api/logout");
		history.push("/");
		dispatch({ type: UNAUTH_USER, payload: false });
	};
}

export function fetchMessage() {
	return function(dispatch) {
		axios
			.get("/", {
				headers: { authorization: localStorage.getItem("token") }
			})
			.then(response => {
				dispatch({
					type: FETCH_MESSAGE,
					payload: response.data.message
				});
			});
	};
}

export function fetchGuider({ userId, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getGuider", { userId, langCode })
			.then(response => {
				dispatch({
					type: FETCH_GUIDER,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchGallery({ userId }) {
	return function(dispatch) {
		return axios
			.post("/api/getUserGallery", { userId })
			.then(response => {
				dispatch({
					type: FETCH_USERGALLERY,
					payload: response
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchTripList({ userId, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getTripList", { userId, langCode })
			.then(response => {
				dispatch({
					type: FETCH_TRIPLIST,
					payload: response
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchTripDetail({ tripId, userId, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/viewTripDetail", { tripId, userId, langCode })
			.then(response => {
				dispatch({
					type: FETCH_TRIPDETAIL,
					payload: response
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function createTrip(props, history) {
	return function(dispatch) {
		axios
			.post("/api/createTrip", props)
			.then(response => {
				dispatch({
					type: CREATE_TRIP,
					payload: response.data
				});
				history.push(`/tripDetail/${response.data.tripId}`);
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function updateTrip(props, history) {
	return function(dispatch) {
		axios
			.post("/api/updateTrip", props)
			.then(response => {
				dispatch({
					type: UPDATE_TRIP,
					payload: response.data
				});
				history.push(`/tripDetail/${response.data._id}`);
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function selectImage(image) {
	return {
		type: SELECTED_IMAGE,
		payload: image
	};
}

export function getGalleryAndFirstImage({ userId }) {
	return (dispatch, getState) => {
		return dispatch(fetchGallery({ userId })).then(() => {
			const gallery = getState().gallery.gallery;
			const firstImage = gallery[0];
			return dispatch(selectImage(firstImage));
		});
	};
}

export function updateProfile(props) {
	return function(dispatch) {
		axios
			.post("/api/updateUserProfile", props)
			.then(response => {
				dispatch({
					type: UPDATE_PROFILE,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchRecentView({ userId, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getRecentView", { userId, langCode })
			.then(response => {
				dispatch({
					type: FETCH_RECENT_VIEW,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchRecommendedGuiders({ langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getRecommandedGuiders", { langCode })
			.then(response => {
				dispatch({
					type: FETCH_RECOMMENDED_GUIDERS,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}
export function fetchRecommendedTrip({ langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getRecommandedTrip", { langCode })
			.then(response => {
				dispatch({
					type: FETCH_RECOMMENDED_TRIP,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchUserProfileInfo({ userId, roleId, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/getUserProfileInfo", { userId, roleId, langCode })
			.then(response => {
				dispatch({
					type: FETCH_USER_PROFILEINFO,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchSearchResult({ locationName, langCode }) {
	return function(dispatch) {
		axios
			.post("/api/searchTrip", { locationName, langCode })
			.then(response => {
				dispatch({
					type: FETCH_SEARCH_RESULT,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchFollwers({ guiderId }) {
	return function(dispatch) {
		axios
			.post("/api/getFollowers", { guiderId })
			.then(response => {
				dispatch({
					type: FETCTH_FOLLOWERS,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchFollows({ touristId }) {
	return function(dispatch) {
		axios
			.post("/api/getFollows", { touristId })
			.then(response => {
				dispatch({
					type: FETCTH_FOLLOWS,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function changeTouristToGuider(props) {
	return function(dispatch) {
		axios
			.post("/api/changeTouristToGuider", props)
			.then(response => {
				dispatch({
					type: CHANGE_TOURIST_TO_GUIDER,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function follow({ touristId, guiderId }) {
	return function(dispatch) {
		axios
			.post("/api/createFollow", { touristId, guiderId })
			.then(response => {
				dispatch({
					type: FOLLOW,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function unfollow({ touristId, guiderId }) {
	return function(dispatch) {
		axios
			.post("/api/unFollow", { touristId, guiderId })
			.then(response => {
				dispatch({
					type: UNFOLLOW,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function fetchChattedMessage({ senderId, receiverId }) {
	return function(dispatch) {
		axios
			.post("/api/getChatMessage", { senderId, receiverId })
			.then(response => {
				dispatch({
					type: FETCH_CHAT_MESSAGE,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}

export function sendMessage({ senderId, receiverId, message }) {
	return function(dispatch) {
		axios
			.post("/api/sendMessage", { senderId, receiverId, message })
			.then(response => {
				dispatch({
					type: SEND_MESSAGE,
					payload: response.data
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
}