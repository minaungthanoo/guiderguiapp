const { User }= require('../models/User');
const { UserInfo }= require('../models/UserInfo');
const jwt = require('jwt-simple');
const config = require('../config/keys');

function tokenForUser(userId) {
    const timestamp = new Date().getTime();
    return jwt.encode( { sub: userId, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
    // User has already had their email and password auth'd
    // We just need to give them a token
    res.send({ token: tokenForUser(req.user.userId), user: req.user });
}

exports.signup = function(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    const name = req.body.name;

    if (!email || !password || !name) {
        return res.status(422).send({ error: 'You must provide user name, email and password'});
    }

    // Check Email Exists
    User.findOne({ email: email }, function(err, existingUser) {
        if (err) { return next(err); }

        // if a user with email exist, return an error
        if (existingUser) {
            return res.status(422).send({ error: 'Email is in use' });
        }

        // if email does not exist, create and save user
        const user = new User({
            email: email,
            password: password
        });

        user.save(async function(err) {
            if (err) {
                return next(err);
            }

            var userObj = {
              userId: user._id,
              name: name
            };

            userObj.profileImg = await UserInfo.createUserInfo(userObj);

            // Set default role as tourist.
            userObj.roleId = 1;
            res.json({ token : tokenForUser(user._id), user: userObj });
        });
    });
};
